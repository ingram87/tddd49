﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

namespace Gomoku
{
    class Controller
    {
        public int id = 0;                                  // move id, start from 1 and increase for each turn
        public int[][] pieces = new int[15][];             // 0(empty) 1(white) 2(black)
        public int x_coord = 0;
        public int y_coord = 0;
        public int turn = 0;                               // if turn = 0(white) 1(black)
        public bool ai_enable = false;
        public int pre_x, pre_y;
        public int ai_x, ai_y;

        public Controller()
        {

        }

        public class Gomoku_move                            // a Gomoku_move class where all the necessary data for each move
        {
            public int id { get; set; }                     // start from 1, indicate move order
            public int turn { get; set; }                   // which player is playing
            public int X { get; set; }                      // Coordinate where the piece is set
            public int Y { get; set; }

            public void Display()
            {
                if (turn == 0)
                {
                    Console.WriteLine("ID: " + id + "Player 1 put a white piece at [" + X + ", " + Y + "]. \n");
                }
                else if (turn == 1)
                {
                    Console.WriteLine("ID: " + id + "Player 2 put a black piece at [" + X + ", " + Y + "]. \n");
                }
            }

        }
        public List<Gomoku_move> Gomoku_move_list = new List<Gomoku_move>();


        public void create_xml_control()
        {
            XDocument gomokuXml = new XDocument(
               new XDeclaration("1.0", "UTF-8", null),
               new XElement("root",
                   from e in Gomoku_move_list
                   select new XElement("move",
                       new XElement("id", e.id),
                       new XElement("turn", e.turn),
                       new XElement("X", e.X),
                       new XElement("Y", e.Y)
                       )
                   )
               );

            gomokuXml.Save("\\gomoku.xml");                                                 // Save data into XML
        }

        public void load_xml_control()
        {
            XDocument doc = XDocument.Load("\\gomoku.xml");        // Define/get data source
            IEnumerable<XElement> moveL =                                                   // Query data from xml doc
                from e in doc.Root.Elements()
                select e;

            foreach (XElement ex in moveL)                                                   // Add data to local list
            {
                Gomoku_move_list.Add(new Gomoku_move() { id = Convert.ToInt32(ex.Element("id").Value), turn = Convert.ToInt32(ex.Element("turn").Value), X = Convert.ToInt32(ex.Element("X").Value), Y = Convert.ToInt32(ex.Element("Y").Value) });
            }
        }

        public void regret_piece_control()
        {
            pieces[x_coord][y_coord] = 0;
            Gomoku_move_list.RemoveAt(Gomoku_move_list.Count - 1);                                      // Remove the item from list
            if (turn == 0)
            {
                turn = 1;
            }
            else if (turn == 1)
            {
                turn = 0;
            }
            id--;
        }


        public bool if_pos_available(int x, int y)
        {
            if ((x >= 0) && (x < 15) && (y >= 0) && (y < 15))               // avoid exception
            {
                if (pieces[x][y] == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }




        public int win_check(int x, int y, int color)      // win check function
        {

            int total_horizontal = 0;                       // horizontal check
            for (int i = x; i > 0; i--)
            {
                if (pieces[i][y] == color)
                {
                    total_horizontal++;
                }
                else
                {
                    break;
                }
            }
            for (int i = x; i < 15; i++)
            {
                if (pieces[i][y] == color)
                {
                    total_horizontal++;
                }
                else
                {
                    break;
                }
            }


            int total_vertical = 0;                         // vertical check
            for (int i = y; i > 0; i--)
            {
                if (pieces[x][i] == color)
                {
                    total_vertical++;
                }
                else
                {
                    break;
                }
            }
            for (int i = y; i < 15; i++)
            {
                if (pieces[x][i] == color)
                {
                    total_vertical++;
                }
                else
                {
                    break;
                }
            }


            int total_diagonal_lr = 0;                      // diagonal left to right check
            int tx = x;
            int ty = y;
            while ((tx > 0) && (ty > 0))
            {
                tx--;
                ty--;
                if (pieces[tx][ty] == color)
                {
                    total_diagonal_lr++;
                }
                else
                {
                    break;
                }
            }
            tx = x;
            ty = y;
            while ((tx < 14) && (ty < 14))
            {
                tx++;
                ty++;
                if (pieces[tx][ty] == color)
                {
                    total_diagonal_lr++;
                }
                else
                {
                    break;
                }
            }


            int total_diagonal_rl = 0;                      // diagonal right to left check
            tx = x;
            ty = y;
            while ((tx > 0) && (ty < 14))
            {
                tx--;
                ty++;
                if (pieces[tx][ty] == color)
                {
                    total_diagonal_rl++;
                }
                else
                {
                    break;
                }
            }
            tx = x;
            ty = y;
            while ((tx < 14) && (ty > 0))
            {
                tx++;
                ty--;
                if (pieces[tx][ty] == color)
                {
                    total_diagonal_rl++;
                }
                else
                {
                    break;
                }
            }
            /// DEBUG MESSAGE
            // MessageBox.Show("h: " + total_horizontal + "; v: " + total_vertical + "; lr: " + total_diagonal_lr + "; rl: " + total_diagonal_rl + "; color: " + color);
            if ((total_horizontal > 5) || (total_vertical > 5) || (total_diagonal_lr > 3) || (total_diagonal_rl > 3))
            {

                if (color == 1)
                {
                    return 1;

                }
                else if (color == 2)
                {
                    return 2;
                }
            }
            return 0;
        }


        public void ai_first_move()
        {

            Random rand = new Random();
            int x = rand.Next(5, 10);
            int y = rand.Next(5, 10);
            if (if_pos_available(x, y))
            {
                pre_x = x;
                pre_y = y;
                ai_random_move(x, y);
            }
        }

        private void ai_random_move(int x, int y)
        {
            int tx = x;
            int ty = y;
            if (tx == 0)
            {
                tx += 7;
            }
            if (ty == 0)
            {
                ty += 6;
            }

            Random rand = new Random();
            tx = rand.Next(tx - 1, tx);
            ty = rand.Next(ty - 1, ty + 1);
            if (if_pos_available(tx, ty))
            {
                ai_x = tx;
                ai_y = ty;
                pre_x = tx;
                pre_y = ty;
                turn = 0;
            }
            else
            {
                ai_random_move(tx, ty);
            }
        }


        public void ai_next_move()
        {

            ai_random_move(pre_x, pre_y);
        }

    }
}
