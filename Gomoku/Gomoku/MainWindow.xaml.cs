﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml;
using System.IO;


namespace Gomoku
{
    /// <summary>
    ///  GOMOKU
    /// </summary>
    /// <summary>
    ///  GOMOKU
    /// Check list: 
    /// Lab1: GUI (done); GUI seperated from other code (done)
    /// Lab2: Game engine (done)
    /// Lab3: AI (done)
    /// Lab4: Save/load (done) Using LINQ (done) using XML (done)
    /// </summary>
    public partial class MainWindow : Window
    {

        static Controller c;

        public MainWindow()
        {
            InitializeComponent();
            c = new Controller();
            init_board();
            Paint();
        }


        /// <summary>
        ///  Gomoku GUI 
        /// </summary>

        public void init_board()
        {
            c.id = 0;
            c.Gomoku_move_list = new List<Controller.Gomoku_move>();
            c.x_coord = 0;
            c.y_coord = 0;
            for (int i = 0; i < 15; i++)
            {
                c.pieces[i] = new int[15];
                for (int j = 0; j < 15; j++)
                {
                    c.pieces[i][j] = 0;                       // make the board empty
                }
            }
        }

        public void Paint()
        {
            for (int i = 0; i < 15; i++)
            {
                Line row_line = new Line();
                Line column_line = new Line();

                row_line.Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                row_line.X1 = 15;                           // 15 pixels margin on each side
                row_line.X2 = 435;
                row_line.Y1 = 30 * i + 15;                  // distance between each line
                row_line.Y2 = 30 * i + 15;

                column_line.Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                column_line.Y1 = 15;
                column_line.Y2 = 435;
                column_line.X1 = 30 * i + 15;
                column_line.X2 = 30 * i + 15;

                Gomoku_Board.Children.Add(row_line);        // paint the lines on board
                Gomoku_Board.Children.Add(column_line);
            }
        }

        public void Paint_piece(int x, int y, int color)
        {
            Ellipse p = new Ellipse();
            p.Width = 20;
            p.Height = 20;

            if (color == 1)
            {
                p.Fill = Brushes.White;
                c.pieces[x][y] = 1;
            }
            else if (color == 2)
            {
                p.Fill = Brushes.Black;
                c.pieces[x][y] = 2;
            }

            Canvas.SetLeft(p, x * 30 + 5);
            Canvas.SetTop(p, y * 30 + 5);
            Gomoku_Board.Children.Add(p);
        }

        public void set_piece(int x, int y, int color)
        {

            if (color == 1)
            {
                Paint_piece(x, y, 1);
            }
            else if (color == 2)
            {
                Paint_piece(x, y, 2);
            }
            ++c.id;
            c.Gomoku_move_list.Add(new Controller.Gomoku_move() { id = c.id, turn = c.turn, X = x, Y = y });
            win_check(x, y, color);
        }


        private void Gomoku_Board_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(Gomoku_Board);                          // Record the mouse click position

            if (((p.X > 0) && (p.X < 450)) && ((p.Y > 0) && (p.Y < 450)))   // only react if the click is within the board
            {
                c.x_coord = (int)Math.Round((p.X - 15) / 30);                 // shift left and top 15 pix, so the piece will be placed where mouse pointed
                c.y_coord = (int)Math.Round((p.Y - 15) / 30);

                if (c.pieces[c.x_coord][c.y_coord] == 0)                          // if no piece was placed on that coord
                {
                    if ((c.turn == 0) && (c.ai_enable == false))                // white player turn
                    {
                        if (c.if_pos_available(c.x_coord, c.y_coord))
                        {
                            set_piece(c.x_coord, c.y_coord, 1);
                            c.turn = 1;                                       // switch side
                        }
                    }
                    else if ((c.turn == 1) && (c.ai_enable == false))           // black player turn
                    {
                        if (c.if_pos_available(c.x_coord, c.y_coord))
                        {
                            set_piece(c.x_coord, c.y_coord, 2);
                            c.turn = 0;
                        }
                    }
                    else if ((c.turn == 0) && (c.ai_enable == true))            // black player ai turn 
                    {
                        if (c.if_pos_available(c.x_coord, c.y_coord))
                        {
                            set_piece(c.x_coord, c.y_coord, 1);
                            c.turn = 1;
                            c.ai_next_move();
                            set_piece(c.ai_x, c.ai_y, 2);
                        }
                    }
                    else
                    {
                        ;                                                   // statement coverage purpose
                    }
                }
                else
                {
                    ;                                                       // statement coverage purpose
                }
            }
        }

        private void new_game_click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("New Game");
            init_board();
            c.ai_enable = false;
            Gomoku_Board.Children.Clear();
            Paint();
        }

        private void new_ai_game_click(object sender, RoutedEventArgs e)
        {
            Gomoku_Board.Children.Clear();
            init_board();
            c.ai_enable = true;
            Paint();
            c.ai_first_move();
        }

        private void restart_game_click(object sender, RoutedEventArgs e)
        {
            if (c.ai_enable)
            {
                Gomoku_Board.Children.Clear();
                init_board();
                Paint();
                c.ai_first_move();
            }
            else
            {
                Gomoku_Board.Children.Clear();
                init_board();
                Paint();
            }
        }

        private void exit_game_click(object sender, RoutedEventArgs e)
        {
            Gomoku_Board.Children.Clear();
            Application.Current.Shutdown();
        }

        private void create_xml_click(object sender, RoutedEventArgs e)
        {
            c.create_xml_control();
        }

        private void load_xml_click(object sender, RoutedEventArgs e)
        {
            c.load_xml_control();

            foreach (Controller.Gomoku_move g in c.Gomoku_move_list)
            {

                if (g.turn == 0)
                {
                    Paint_piece(g.X, g.Y, 1);
                    c.turn = 1;
                }
                else if (g.turn == 1)
                {
                    Paint_piece(g.X, g.Y, 2);
                    c.turn = 0;
                }
                ++c.id;
                c.win_check(g.X, g.Y, g.turn + 1);

            }

        }

        private void regret_piece_click(object sender, RoutedEventArgs e)
        {
            Gomoku_Board.Children.Remove(Gomoku_Board.Children[Gomoku_Board.Children.Count - 1]);       // remove the piece from board
            c.regret_piece_control();
        }


        private void win_check(int x, int y, int color)
        {
            if (c.win_check(x, y, color) == 1)
            {
                c.Gomoku_move_list = new List<Controller.Gomoku_move>();
                MessageBox.Show("White win");
                Gomoku_Board.Children.Clear();
                init_board();
                Paint();
            }
            else if (c.win_check(x, y, color) == 2)
            {
                c.Gomoku_move_list = new List<Controller.Gomoku_move>();
                MessageBox.Show("Black win");
                Gomoku_Board.Children.Clear();
                init_board();
                Paint();
            }
        }

        /* -------------------------------------------------------------------------------- */

    }
}
